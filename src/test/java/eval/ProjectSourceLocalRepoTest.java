package eval;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.junit.Assert.*;

import java.io.IOException;

import org.junit.Test;

import data.ProjectSourceLocalRepo;
import eval.Configurations;

public class ProjectSourceLocalRepoTest {

	@Test
	public void test() throws IOException {
		ProjectSourceLocalRepo dataset = new ProjectSourceLocalRepo(Configurations.PROJECT_DATA_DIR);
		dataset.save("123", "<xml></xml>");
		assertThat(dataset.get("123"), is(notNullValue()));
	}

}
