package eval;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.collect.Lists;

import data.db.ProjectDatabaseException;
import data.db.ProjectMongoDataset;
import data.model.Key;
import data.model.ProjectIdBasedResult;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProjectIdBasedResultTest {

	@Test
	public void testProjectIdPrimiaryKeyBasedResult() throws JsonProcessingException, ProjectDatabaseException {
		ProjectMongoDataset beforeMetricsColl = new ProjectMongoDataset("testdb", "before");
		beforeMetricsColl.drop();
		ProjectMongoDataset afterMetricsColl = new ProjectMongoDataset("testdb", "after");
		afterMetricsColl.drop();

		// test entry # 1
		beforeMetricsColl.put(new ProjectIdBasedResult("1")
				.append(Key.BLOCKS, 10)
				.append(Key.COMPLEXITY, 5)
				.append(Key.AVG_SCRIPT_LENGTH, 8.5).toDoc());

		afterMetricsColl.put(new ProjectIdBasedResult("1")
				.append(Key.BLOCKS, 6)
				.append(Key.COMPLEXITY, 2)
				.append(Key.AVG_SCRIPT_LENGTH, 5.5).toDoc());

		// test entry # 2

		beforeMetricsColl.put(new ProjectIdBasedResult("2")
				.append(Key.BLOCKS, 20)
				.append(Key.COMPLEXITY, 10)
				.append(Key.AVG_SCRIPT_LENGTH, 10).toDoc());

		afterMetricsColl.put(new ProjectIdBasedResult("2")
				.append(Key.BLOCKS, 15)
				.append(Key.COMPLEXITY, 8)
				.append(Key.AVG_SCRIPT_LENGTH, 8).toDoc());

		// test entry # 3

		beforeMetricsColl.put(new ProjectIdBasedResult("3")
				.append(Key.BLOCKS, 30)
				.append(Key.COMPLEXITY, 10)
				.append(Key.AVG_SCRIPT_LENGTH, 10).toDoc());

		// test intentionally left out afterMetrics result for test project id 3 (when
		// no refactoring could be applied)
	}

	@Test
	public void testResultIdAsPrimaryWithProjectIdLinkResult()
			throws ProjectDatabaseException, JsonProcessingException {
		ProjectMongoDataset smellResults = new ProjectMongoDataset("testdb", "smell-duplicate_code");
		ProjectMongoDataset refactResults = new ProjectMongoDataset("testdb", "refact-extract_procedure");
		smellResults.drop();
		refactResults.drop();
		smellResults.put(new ProjectIdBasedResult("smellId1", "1")
				.append(Key.INSTANCE_SIZE, 10)
				.append(Key.GROUP_SIZE, 4)
				.toDoc());

		refactResults.put(new ProjectIdBasedResult("smellId1", "1")
				.append(Key.FAILED_PRECOND_1, true)
				.toDoc());

		smellResults.put(new ProjectIdBasedResult("smellId2", "1")
				.append(Key.INSTANCE_SIZE, 5)
				.append(Key.GROUP_SIZE, 3)
				.toDoc());

		refactResults.put(new ProjectIdBasedResult("smellId2", "1")
				.append(Key.NUM_PARAMETER, 1)
				.toDoc());

		smellResults.put(new ProjectIdBasedResult("smellId3", "2")
				.append(Key.INSTANCE_SIZE, 8)
				.append(Key.GROUP_SIZE, 4)
				.toDoc());

		refactResults.put(new ProjectIdBasedResult("smellId3", "2")
				.append(Key.FAILED_PRECOND_1, true)
				.append(Key.FAILED_PRECOND_2, true)
				.toDoc());

		smellResults.put(new ProjectIdBasedResult("smellId4", "2")
				.append(Key.INSTANCE_SIZE, 5)
				.append(Key.GROUP_SIZE, 5)
				.toDoc());

		refactResults.put(new ProjectIdBasedResult("smellId4", "2")
				.append(Key.NUM_PARAMETER, 3)
				.toDoc());

	}

}
