package refact.eval.runner;

import java.util.ArrayList;
import java.util.List;

import org.bson.Document;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import lombok.extern.slf4j.Slf4j;
import refact.eval.ProjectMetadata;
import refact.eval.ProjectMongoDataset;
import refact.eval.ProjectSourceLocalRepo;
import refact.eval.util.Utility;

@Slf4j
public class ProjectRemixProcessor extends AbstractDataProcessor {
	List<String> remixList;
	ProjectMongoDataset resultColl;

	public ProjectRemixProcessor(ProjectMongoDataset inputDataset, ProjectMongoDataset outputDataset,
			ProjectSourceLocalRepo projectXmlRepo) throws Exception {
		super(inputDataset, outputDataset, projectXmlRepo);
		resultColl = new ProjectMongoDataset("iseud19", "remixes");
		remixList = new ArrayList<String>();
	}

	@Override
	public void processProject(String projectId) {

		JsonParser parser = new JsonParser();
		String url = String.format("https://scratch.mit.edu/projects/%s/remixtree/bare/", projectId);
		try {
			String jsonStr = Utility.httpGetRequest(url);
			JsonObject remixes = parser.parse(jsonStr).getAsJsonObject();
			int count = 0;
			ArrayList<String> remixColl = new ArrayList<String>();
			for(String eachRemix : remixes.keySet()) {
				if(!eachRemix.equals("root_id") && !remixes.get(eachRemix).getAsJsonObject().get("parent_id").isJsonNull() && remixes.get(eachRemix).getAsJsonObject().get("parent_id").getAsString().equals(projectId)) {
					++count;
					JsonObject eachRemixObj = remixes.get(eachRemix).getAsJsonObject();
//					log.info(eachRemix);
					remixColl.add(eachRemix);
					remixList.add(new ProjectMetadata(eachRemix,eachRemixObj.get("username").getAsString(),eachRemixObj.get("title").getAsString()).toJsonStr());
				}
				if(count>4) break;
			}
//			log.info(""+count);
			Document doc = new Document("remixes", remixColl);
			String jsonString = doc.toJson();
			resultColl.put(projectId, jsonString);
		} catch (Exception e) {
			log.error("Failure getting top 5 remixes for {} {} {}", projectId, e.getMessage(), e.getStackTrace());
		}
	}

	public static void main(String[] args) throws Exception {
		ProjectMongoDataset inputDataset = new ProjectMongoDataset("iseud19", "projects");
		ProjectRemixProcessor projectRemixProcessor = new ProjectRemixProcessor(inputDataset, null, null);
		projectRemixProcessor.run();
		for(String eachRemix : projectRemixProcessor.remixList) {
			inputDataset.put(eachRemix);
		}
	}

}
