package data.db;

import org.bson.Document;
import org.bson.conversions.Bson;

import com.mongodb.client.FindIterable;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProjectMongoDataset extends MongoDataset {

	public ProjectMongoDataset(String databaseName, String collectionName) throws ProjectDatabaseException {
		super(databaseName, collectionName);
	}

	public void put(String jsonString) {
		Document doc = Document.parse(jsonString);
		if (!doc.containsKey("_id")) {
			log.warn(
					"Field _id should be provided to properly store the result (normally _id corresponds to projectId)");
		}
		put(doc);
	}

	public void put(String id, String jsonString) {
		Document doc = Document.parse(jsonString);
		doc.putIfAbsent("_id", id);
		put(doc);
	}

	/**
	 * upsert
	 * 
	 * @param doc
	 */
	public void put(Document doc) {
		UpdateOptions options = new UpdateOptions().upsert(true);
		Bson filter = Filters.eq("_id", doc.getString("_id"));
		collection.replaceOne(filter, doc, options);
	}

	public long size() {
		return collection.count();
	}

	public void drop() {
		collection.drop();
	}

	public Document get(String id) {
		Bson filter = Filters.eq("_id", id);
		FindIterable<Document> results = collection.find(filter);
		return results.first();
	}

	public FindIterable<Document> getAll() {
		return collection.find();
	}
}
