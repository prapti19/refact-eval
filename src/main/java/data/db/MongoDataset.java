package data.db;

import org.bson.Document;

import com.mongodb.client.MongoCollection;

public class MongoDataset {
	protected MongoCollection<Document> collection;

	public MongoDataset(String databaseName, String collectionName) throws ProjectDatabaseException {
		collection = new DBConnection(databaseName, collectionName).getCollection();
	}
}
