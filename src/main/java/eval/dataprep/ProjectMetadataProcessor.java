package eval.dataprep;

import java.io.File;
import java.io.IOException;

import data.db.ProjectMongoDataset;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProjectMetadataProcessor {
	String PROJECT_LIST_JSON_FILE_PATH = "/home/prapti/refact-eval/src/main/resources/top5-explore-trending-01-25-2019.json";
	private ScratchProjectJsonArrayReader reader;

	public ProjectMetadataProcessor() throws IOException {
		File file = new File(PROJECT_LIST_JSON_FILE_PATH);
		reader = new ScratchProjectJsonArrayReader(file);
	}

	public static void main(String[] args) throws IOException {
		ProjectMongoDataset resultColl;
		try {
			resultColl = new ProjectMongoDataset("iseud19", "projects");
			ProjectMetadataProcessor collector = new ProjectMetadataProcessor();
			for (int i = 0; i < collector.reader.getSize(); i++) {
				resultColl.put(collector.reader.getProject(i).toJsonStr());
			}
		} catch (Exception e) {
			log.error(e.getMessage());
		}

	}
}
