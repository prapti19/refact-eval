package eval.dataprep;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import data.ProjectSourceLocalRepo;
import data.db.ProjectMongoDataset;
import eval.AbstractDataProcessor;
import eval.Configurations;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ProjectXmlProcessor extends AbstractDataProcessor {

	public ProjectXmlProcessor(ProjectMongoDataset inputDataset, ProjectSourceLocalRepo projectXmlRepo)
			throws Exception {
		super(inputDataset, null, projectXmlRepo);
	}

	@Override
	public void showProgress(String projectId) {
		log.info("{} - Processed:{}, {}/{}", counter.incrementAndGet(), projectId, projectXmlRepo.size(),
				inputDataset.size());
	}

	@Override
	public void processProject(String projectId) {
		if (projectXmlRepo.contains(projectId)) {
			return;
		}

//		String url = String.format("http://localhost:8080/projects/%s/xml", projectId);
		String url = String.format("https://tidyblocks.appspot.com/projects/%s/xml", projectId);
		try {
			String xmlStr = httpGetRequest(url);
			projectXmlRepo.save(projectId, xmlStr);
		} catch (Exception e) {
			log.error("Failure XML extraction for {} {} {}", projectId, e.getMessage(), e.getStackTrace());
		}
	}

	public static String httpGetRequest(String urlToRead) throws Exception {
		StringBuilder result = new StringBuilder();
		URL url = new URL(urlToRead);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setRequestMethod("GET");
		BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		String line;
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		rd.close();
		return result.toString();
	}

	public static void main(String[] args) throws Exception {
		ProjectSourceLocalRepo xmlDataset = new ProjectSourceLocalRepo(Configurations.PROJECT_DATA_DIR);
		ProjectMongoDataset inputDataset = new ProjectMongoDataset("iseud19", "projects");
		new ProjectXmlProcessor(inputDataset, xmlDataset).run();
	}
}
