package eval;

import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import org.bson.Document;

import com.mongodb.client.FindIterable;

import data.ProjectSourceLocalRepo;
import data.db.ProjectMongoDataset;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public abstract class AbstractDataProcessor {
	protected final AtomicInteger counter;
	protected final ExecutorService executor;
	protected final Stream<String> projectIdStream;
	protected final ProjectMongoDataset inputDataset;
	protected final ProjectSourceLocalRepo projectXmlRepo;
	protected final ProjectMongoDataset outputDataset;

	public AbstractDataProcessor(ProjectMongoDataset inputDataset, ProjectMongoDataset outputDataset,
			ProjectSourceLocalRepo projectXmlRepo) {
		this.inputDataset = inputDataset;
		this.counter = new AtomicInteger(0);
		this.executor = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
		this.projectXmlRepo = projectXmlRepo;
		this.projectIdStream = getProjectIdStream();
		this.outputDataset = outputDataset;

	}

	public void run() throws InterruptedException {
		List<Callable<String>> projectIdCallables = generateCallableList();
		executor.invokeAll(projectIdCallables).stream().map(future -> {
			try {
				String processedId = future.get();
				showProgress(processedId);
				return processedId;
			} catch (Exception e) {
				throw new IllegalStateException(e);
			}
		});
		executor.shutdown();
	}

	public List<Callable<String>> generateCallableList() {
		List<Callable<String>> projectIdCallables = projectIdStream.map(projectId -> (Callable<String>) (() -> {
			processProject(projectId);
			showProgress(projectId);
			return projectId;
		})).collect(Collectors.toList());

		return projectIdCallables;
	}

	public abstract void processProject(String projectId);

	public void showProgress(String projectId) {
		log.info("processed {}", projectId);
	};

	public Stream<String> getProjectIdStream() {
		if (inputDataset == null) {
			log.error("Need to provide projectId stream or ProjectMongoDataset inputDataset");
			return Stream.empty();
		}
		return extractProjectIdStream(inputDataset);
	}

	public static Stream<String> extractProjectIdStream(ProjectMongoDataset inputDataset) {
		FindIterable<Document> datasetIterable = inputDataset.getAll();
		Iterable<Document> projectIterable = () -> datasetIterable.iterator();
		Stream<Document> docStream = StreamSupport.stream(projectIterable.spliterator(), true);
		Stream<String> projectIdStream = docStream.map(doc -> doc.getString("_id"));
		return projectIdStream;
	}

}
