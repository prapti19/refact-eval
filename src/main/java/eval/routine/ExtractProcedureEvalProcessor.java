package eval.routine;

import java.io.IOException;
import java.util.Map;

import anlys.metrics.MetricsKeyValue;
import anlys.metrics.ProgramMetricsAnalyzer;
import anlys.smell.DuplicateCode;
import anlys.smell.DuplicateCodeAnalyzer;
import anlys.smell.SmellListResult;
import ast.Program;
import data.ProjectSourceLocalRepo;
import data.db.ProjectMongoDataset;
import eval.AbstractDataProcessor;
import eval.Configurations;
import sb3.parser.xml.Sb3XmlParser;

public class ExtractProcedureEvalProcessor extends AbstractDataProcessor {

	public ExtractProcedureEvalProcessor(ProjectMongoDataset inputDataset, ProjectMongoDataset outputDataset,
			ProjectSourceLocalRepo projectXmlRepo) {
		super(inputDataset, outputDataset, projectXmlRepo);
	}

	@Override
	public void processProject(String projectId) {
		try {
			String xmlStr = projectXmlRepo.get(projectId);
			Program program = new Sb3XmlParser().parseProgram(xmlStr);

			// TODO: metrics of program before
			Map<String, Object> metricsBefore = computeQualityMetricsBefore(program);

			// TODO: multipass smell-refactoring
			DuplicateCodeAnalyzer analyzer = new DuplicateCodeAnalyzer();
			SmellListResult<DuplicateCode> results = analyzer.analyze(program);
			String jsonResult = results.getMetadataAsJson();

			// TODO: metrics of program after

			outputDataset.put(projectId, jsonResult);
		} catch (IOException e) {
			// TODO throw processing error & log problematic project id
			e.printStackTrace();
		}

	}

	private Map<String, Object> computeQualityMetricsBefore(Program program) {
		ProgramMetricsAnalyzer analyzer = new ProgramMetricsAnalyzer();
		MetricsKeyValue metrics = analyzer.computeMetrics(program).getProjectMetricsInfo();
		return metrics.asMap();
	}

	public static void main(String[] args) throws Exception {
		ProjectSourceLocalRepo xmlDataset = new ProjectSourceLocalRepo(Configurations.PROJECT_DATA_DIR);
		ProjectMongoDataset inputDataset = new ProjectMongoDataset("iseud19", "projects");
		ProjectMongoDataset outputDataset = new ProjectMongoDataset("iseud19", "duplicate_code");
		ExtractProcedureEvalProcessor processor = new ExtractProcedureEvalProcessor(inputDataset, outputDataset, xmlDataset);
		processor.run();
	}

}
